import { Storyline } from "@tripetto/runner";
import { IBlockRenderer } from "./blocks";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import BackIcon from "@mui/icons-material/NavigateBeforeRounded";
import NextIcon from "@mui/icons-material/NavigateNextRounded";
import CompleteIcon from "@mui/icons-material/CheckCircleRounded";

export const Buttons = (props: { storyline: Storyline<IBlockRenderer> }) => (
    <Grid item sx={{ mt: 2 }}>
        <Grid container spacing={8} justifyContent="space-between">
            {props.storyline.mode === "progressive" ? (
                <Grid item>
                    <Button
                        variant="contained"
                        color="primary"
                        disabled={!props.storyline.isFinishable}
                        onClick={() => props.storyline.finish()}
                    >
                        <CompleteIcon style={{ marginRight: 6 }} />
                        Complete
                    </Button>
                </Grid>
            ) : (
                <>
                    <Grid item>
                        <Button
                            variant="contained"
                            color="primary"
                            disabled={props.storyline.isFailed || (props.storyline.isAtFinish && !props.storyline.isFinishable)}
                            onClick={() => props.storyline.stepForward()}
                        >
                            {props.storyline.isAtFinish ? <CompleteIcon style={{ marginRight: 6 }} /> : <NextIcon />}
                            {props.storyline.isAtFinish ? "Complete" : "Next"}
                        </Button>
                    </Grid>
                    {!props.storyline.isAtStart && (
                        <Grid item>
                            <Button
                                variant="contained"
                                color="inherit"
                                disabled={props.storyline.isAtStart}
                                onClick={() => props.storyline.stepBackward()}
                            >
                                <BackIcon />
                                Back
                            </Button>
                        </Grid>
                    )}
                </>
            )}
        </Grid>
    </Grid>
);
