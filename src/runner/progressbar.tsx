import Grid from "@mui/material/Grid";
import LinearProgress from "@mui/material/LinearProgress";

export const Progressbar = (props: { percentage: number }) => (
    <Grid item sx={{ mt: 2 }}>
        <LinearProgress variant="determinate" value={props.percentage} />
    </Grid>
);
