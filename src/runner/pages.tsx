import { IPage } from "@tripetto/runner";
import Grid from "@mui/material/Grid";
import ButtonGroup from "@mui/material/ButtonGroup";
import Button from "@mui/material/Button";

export const Pages = (props: { pages: IPage[] }) =>
    (props.pages.length > 0 && (
        <Grid item sx={{ mt: 2 }}>
            <ButtonGroup size="small">
                {props.pages.map((page) => (
                    <Button key={page.number} variant={page.active ? "contained" : "outlined"} onClick={() => page.activate()}>
                        {page.number}
                    </Button>
                ))}
            </ButtonGroup>
        </Grid>
    )) || <></>;
