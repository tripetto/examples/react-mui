import { tripetto } from "@tripetto/runner";
import { ReactNode } from "react";
import { Email } from "@tripetto/block-email/runner";
import { IBlockRenderer, IBlockProps } from ".";
import InputAdornment from "@mui/material/InputAdornment";
import TextField from "@mui/material/TextField";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-email",
})
export class EmailRenderer extends Email implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <>
                {props.name(this.required)}
                {props.description}
                <TextField
                    sx={{ mt: 1 }}
                    key={this.key()}
                    type="email"
                    margin="normal"
                    fullWidth
                    required={this.required}
                    value={this.emailSlot.value}
                    label={props.placeholder}
                    helperText={props.explanation}
                    InputProps={{
                        startAdornment: <InputAdornment position="start">@</InputAdornment>,
                    }}
                    onChange={(ev) => {
                        this.emailSlot.value = ev.target.value;
                    }}
                    onBlur={(ev) => {
                        ev.target.value = this.emailSlot.string;
                    }}
                />
            </>
        );
    }
}
