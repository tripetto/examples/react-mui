import { tripetto, NodeBlock, assert } from "@tripetto/runner";
import { ReactNode } from "react";
import { IBlockRenderer, IBlockProps } from ".";

@tripetto({
    type: "node",
    identifier: "example",
})
export class TextBlock extends NodeBlock implements IBlockRenderer {
    readonly exampleSlot = assert(this.valueOf("example-slot"));
    readonly required = this.exampleSlot.slot.required || false;

    render(props: IBlockProps): ReactNode {
        return (
            <>
                {props.name(this.required)}
                {props.description}
                <div
                    onClick={() => this.exampleSlot.set("A nice value!")}
                    style={{
                        color: "red",
                    }}
                >
                    This is an example block with an example data slot that can be set. If the block is required, the validation will pass
                    as soon as the value is set.
                    <br />
                    Current value of example slot: <b>{this.exampleSlot.string || "Not set"}</b> (click here to set a value)
                </div>
                {props.explanation}
            </>
        );
    }
}
