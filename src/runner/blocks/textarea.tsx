import { tripetto } from "@tripetto/runner";
import { ReactNode } from "react";
import { Textarea } from "@tripetto/block-textarea/runner";
import { IBlockRenderer, IBlockProps } from ".";
import TextField from "@mui/material/TextField";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-textarea",
})
export class TextareaRenderer extends Textarea implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <>
                {props.name(this.required)}
                {props.description}
                <TextField
                    multiline
                    sx={{ mt: 1 }}
                    key={this.key()}
                    type="text"
                    margin="normal"
                    fullWidth
                    rows="3"
                    required={this.required}
                    value={this.textareaSlot.value}
                    label={props.placeholder}
                    helperText={props.explanation}
                    inputProps={{
                        maxLength: this.maxLength,
                    }}
                    onChange={(ev) => {
                        this.textareaSlot.value = ev.target.value;
                    }}
                    onBlur={(ev) => {
                        ev.target.value = this.textareaSlot.string;
                    }}
                />
            </>
        );
    }
}
