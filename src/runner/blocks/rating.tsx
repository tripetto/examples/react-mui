import { tripetto } from "@tripetto/runner";
import { ReactNode } from "react";
import { Rating as RatingBlock } from "@tripetto/block-rating/runner";
import { IBlockRenderer, IBlockProps } from ".";
import Rating from "@mui/material/Rating";
import FormGroup from "@mui/material/FormGroup";
import FormHelperText from "@mui/material/FormHelperText";
import StarIcon from "@mui/icons-material/Star";
import StarBorderIcon from "@mui/icons-material/StarBorder";
import FavoriteIcon from "@mui/icons-material/Favorite";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import ThumbUpIcon from "@mui/icons-material/ThumbUp";
import ThumbUpBorderIcon from "@mui/icons-material/ThumbUpOutlined";
import ThumbDownIcon from "@mui/icons-material/ThumbDown";
import ThumbDownBorderIcon from "@mui/icons-material/ThumbDownOutlined";
import PersonIcon from "@mui/icons-material/Person";
import PersonBorderIcon from "@mui/icons-material/Person2Outlined";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-rating",
})
export class RatingRenderer extends RatingBlock implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <>
                {this.props.imageURL && this.props.imageAboveText && (
                    <div>
                        <img src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} />
                    </div>
                )}
                {props.name(this.required)}
                {props.description}
                {this.props.imageURL && !this.props.imageAboveText && (
                    <div>
                        <img src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} />
                    </div>
                )}
                <FormGroup>
                    <Rating
                        sx={{ mt: 1 }}
                        value={this.ratingSlot.value}
                        max={this.props.steps || 5}
                        name={this.key()}
                        size="large"
                        icon={
                            this.props.shape === "hearts" ? (
                                <FavoriteIcon fontSize="inherit" />
                            ) : this.props.shape === "thumbs-up" ? (
                                <ThumbUpIcon fontSize="inherit" />
                            ) : this.props.shape === "thumbs-down" ? (
                                <ThumbDownIcon fontSize="inherit" />
                            ) : this.props.shape === "persons" ? (
                                <PersonIcon fontSize="inherit" />
                            ) : undefined
                        }
                        emptyIcon={
                            this.props.shape === "hearts" ? (
                                <FavoriteBorderIcon fontSize="inherit" />
                            ) : this.props.shape === "thumbs-up" ? (
                                <ThumbUpBorderIcon fontSize="inherit" />
                            ) : this.props.shape === "thumbs-down" ? (
                                <ThumbDownBorderIcon fontSize="inherit" />
                            ) : this.props.shape === "persons" ? (
                                <PersonBorderIcon fontSize="inherit" />
                            ) : undefined
                        }
                        onChange={(ev, value) => {
                            this.ratingSlot.pristine = value || undefined;
                        }}
                    />
                    {props.explanation && <FormHelperText sx={{ ml: 0 }}>{props.explanation}</FormHelperText>}
                </FormGroup>
            </>
        );
    }
}
