import { tripetto } from "@tripetto/runner";
import { ReactNode } from "react";
import { Checkboxes, ICheckbox } from "@tripetto/block-checkboxes/runner";
import { IBlockRenderer, IBlockProps } from ".";
import FormControl from "@mui/material/FormControl";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormHelperText from "@mui/material/FormHelperText";
import Checkbox from "@mui/material/Checkbox";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-checkboxes",
})
export class CheckboxesRenderer extends Checkboxes implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <>
                {props.name()}
                {props.description}
                <FormControl>
                    <FormGroup>
                        {this.checkboxes(props).map(
                            (checkbox) =>
                                checkbox.label && (
                                    <FormControlLabel
                                        key={this.key(checkbox.id)}
                                        control={<Checkbox checked={this.isChecked(checkbox)} onChange={() => this.toggle(checkbox)} />}
                                        label={checkbox.label}
                                    />
                                )
                        )}
                    </FormGroup>
                    {props.explanation && <FormHelperText sx={{ ml: 0 }}>{props.explanation}</FormHelperText>}
                </FormControl>
            </>
        );
    }
}
