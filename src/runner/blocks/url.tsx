import { tripetto } from "@tripetto/runner";
import { ReactNode } from "react";
import { URL } from "@tripetto/block-url/runner";
import { IBlockRenderer, IBlockProps } from ".";
import TextField from "@mui/material/TextField";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-url",
})
export class URLRenderer extends URL implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <>
                {props.name(this.required)}
                {props.description}
                <TextField
                    sx={{ mt: 1 }}
                    key={this.key()}
                    type="url"
                    margin="normal"
                    fullWidth
                    required={this.required}
                    value={this.urlSlot.value}
                    label={props.placeholder || "https://"}
                    helperText={props.explanation}
                    onChange={(ev) => {
                        this.urlSlot.value = ev.target.value;
                    }}
                    onBlur={(ev) => {
                        ev.target.value = this.urlSlot.string;
                    }}
                />
            </>
        );
    }
}
