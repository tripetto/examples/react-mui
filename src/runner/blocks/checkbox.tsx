import { tripetto } from "@tripetto/runner";
import { ReactNode } from "react";
import { Checkbox } from "@tripetto/block-checkbox/runner";
import { IBlockRenderer, IBlockProps } from ".";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormGroup from "@mui/material/FormGroup";
import FormHelperText from "@mui/material/FormHelperText";
import Switch from "@mui/material/Switch";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-checkbox",
})
export class CheckboxRenderer extends Checkbox implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <>
                {props.placeholder && props.name(this.required)}
                {props.description}
                <FormGroup>
                    <FormControlLabel
                        control={
                            <Switch
                                key={this.key()}
                                checked={this.checkboxSlot.value}
                                onChange={(ev, isChecked) => {
                                    this.checkboxSlot.value = isChecked;
                                }}
                            />
                        }
                        label={props.placeholder || props.label(this.required)}
                    />
                    {props.explanation && <FormHelperText sx={{ ml: 0 }}>{props.explanation}</FormHelperText>}
                </FormGroup>
            </>
        );
    }
}
