import { tripetto, castToString } from "@tripetto/runner";
import { ReactNode } from "react";
import { Number } from "@tripetto/block-number/runner";
import { IBlockRenderer, IBlockProps } from ".";
import InputAdornment from "@mui/material/InputAdornment";
import TextField from "@mui/material/TextField";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-number",
})
export class NumberRenderer extends Number implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <>
                {props.name(this.required)}
                {props.description}
                <TextField
                    sx={{ mt: 1 }}
                    key={this.key()}
                    type="text"
                    margin="normal"
                    fullWidth
                    required={this.required}
                    value={(this.numberSlot.hasValue && this.numberSlot.string) || ""}
                    label={props.placeholder}
                    helperText={props.explanation}
                    InputProps={{
                        startAdornment: <InputAdornment position="start">#</InputAdornment>,
                    }}
                    onChange={(ev) => {
                        this.numberSlot.pristine = ev.target.value;
                    }}
                    onFocus={(ev: React.FocusEvent<HTMLInputElement>) => {
                        const el = ev.target;

                        /** In Firefox we lose focus when switching input type. */
                        requestAnimationFrame(() => {
                            el.focus();
                        });

                        // Switch to number type when focus is gained.
                        el.value = castToString(this.numberSlot.value);
                        el.type = "number";
                    }}
                    onBlur={(ev: React.FocusEvent<HTMLInputElement>) => {
                        const el = ev.target;

                        // Switch to text type to allow number prefix and suffix.
                        el.type = "text";
                        el.value = this.numberSlot.string;
                    }}
                />
            </>
        );
    }
}
