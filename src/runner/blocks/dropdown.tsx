import { tripetto } from "@tripetto/runner";
import { ReactNode } from "react";
import { Dropdown } from "@tripetto/block-dropdown/runner";
import { IBlockRenderer, IBlockProps } from ".";
import MenuItem from "@mui/material/MenuItem";
import TextField from "@mui/material/TextField";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-dropdown",
})
export class DropdownRenderer extends Dropdown implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <>
                {props.name(this.required)}
                {props.description}
                <TextField
                    sx={{ mt: 1 }}
                    key={this.key()}
                    select
                    margin="normal"
                    fullWidth
                    required={this.required}
                    label={props.placeholder}
                    value={this.value}
                    onChange={(ev) => {
                        this.value = ev.target.value;
                    }}
                    helperText={props.explanation}
                >
                    {this.options.map(
                        (option) =>
                            option.name && (
                                <MenuItem key={option.id} value={option.id}>
                                    {option.name}
                                </MenuItem>
                            )
                    )}
                </TextField>
            </>
        );
    }
}
