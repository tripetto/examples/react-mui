import { tripetto } from "@tripetto/runner";
import { ReactNode } from "react";
import { Password } from "@tripetto/block-password/runner";
import { IBlockRenderer, IBlockProps } from ".";
import TextField from "@mui/material/TextField";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-password",
})
export class PasswordRenderer extends Password implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <>
                {props.name(this.required)}
                {props.description}
                <TextField
                    sx={{ mt: 1 }}
                    key={this.key()}
                    type="password"
                    margin="normal"
                    fullWidth
                    required={this.required}
                    value={this.passwordSlot.value}
                    label={props.placeholder}
                    helperText={props.explanation}
                    onChange={(ev) => {
                        this.passwordSlot.value = ev.target.value;
                    }}
                    onBlur={(ev) => {
                        ev.target.value = this.passwordSlot.string;
                    }}
                />
            </>
        );
    }
}
