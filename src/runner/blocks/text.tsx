import { tripetto } from "@tripetto/runner";
import { ReactNode } from "react";
import { Text } from "@tripetto/block-text/runner";
import { IBlockRenderer, IBlockProps } from ".";
import TextField from "@mui/material/TextField";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-text",
})
export class TextRenderer extends Text implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <>
                {props.name(this.required)}
                {props.description}
                <TextField
                    sx={{ mt: 1 }}
                    key={this.key()}
                    type="text"
                    fullWidth
                    required={this.required}
                    value={this.textSlot.value}
                    label={props.placeholder}
                    helperText={props.explanation}
                    autoComplete={this.props.autoComplete || "off"}
                    inputMode={this.props.autoComplete === "tel" ? "tel" : "text"}
                    inputProps={{
                        maxLength: this.maxLength,
                        list: (this.props.suggestions && this.key("list")) || undefined,
                    }}
                    onChange={(ev) => {
                        this.textSlot.value = ev.target.value;
                    }}
                    onBlur={(ev) => {
                        ev.target.value = this.textSlot.string;
                    }}
                />
                {this.props.suggestions && (
                    <datalist id={this.key("list")}>
                        {this.props.suggestions.map((suggestion, index) => suggestion && <option key={index} value={suggestion.name} />)}
                    </datalist>
                )}
            </>
        );
    }
}
