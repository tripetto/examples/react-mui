import { tripetto } from "@tripetto/runner";
import { ReactNode } from "react";
import { Radiobuttons } from "@tripetto/block-radiobuttons/runner";
import { IBlockRenderer, IBlockProps } from ".";
import FormControl from "@mui/material/FormControl";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormHelperText from "@mui/material/FormHelperText";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-radiobuttons",
})
export class RadiobuttonsRenderer extends Radiobuttons implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <>
                {props.name(this.required)}
                {props.description}
                <FormControl>
                    <RadioGroup
                        value={this.value}
                        onChange={(ev, value) => {
                            this.value = value;
                        }}
                    >
                        {this.buttons(props).map((radiobutton) => (
                            <FormControlLabel
                                key={this.key(radiobutton.id)}
                                value={radiobutton.id}
                                control={<Radio />}
                                label={radiobutton.name}
                            />
                        ))}
                    </RadioGroup>
                    {props.explanation && <FormHelperText sx={{ ml: 0 }}>{props.explanation}</FormHelperText>}
                </FormControl>
            </>
        );
    }
}
