import styled from "@emotion/styled";
import { ReactNode } from "react";
import { L10n, TModes } from "@tripetto/runner";

const PageElement = styled.div`
    display: flex;
    align-items: center;
    width: 100%;
    margin-top: 24px;

    &:first-of-type {
        margin-top: 0;
        margin-bottom: 24px;
    }

    > div {
        flex-grow: 1;

        &:nth-of-type(2) {
            flex-grow: 0;
            white-space: nowrap;
            padding: 0 1em;
            font-size: 11px;
            font-family: Tahoma, Arial;
            text-transform: uppercase;
            color: #000;
            opacity: 0.5;
            cursor: default;
        }

        &:first-of-type,
        &:last-of-type {
            height: 1px;
            border-bottom: 1px dashed #000;
            opacity: 0.25;
        }
    }
`;

export const Page = (props: {
    readonly page: number;
    readonly mode: TModes;
    readonly title?: string;
    readonly isPreview: boolean;
    readonly children?: ReactNode;
}) =>
    props.isPreview ? (
        <>
            <PageElement>
                <div></div>
                <div>
                    {props.mode === "paginated" ? "Page" : "Section"} {L10n.Locales.number(props.page + 1)}
                    {(props.title && ` - ${props.title}`) || ""}
                </div>
                <div></div>
            </PageElement>
            {props.children}
        </>
    ) : (
        <>{props.children}</>
    );
