import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import FormControl from "@mui/material/FormControl";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormGroup from "@mui/material/FormGroup";
import Switch from "@mui/material/Switch";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import { TModes } from "@tripetto/runner";

export const SettingsDialog = (props: {
    readonly mode: TModes;
    readonly enumerators: boolean;
    readonly pages: boolean;
    readonly progressbar: boolean;
    readonly setMode: (mode: TModes) => void;
    readonly setEnumerators: (enumerators: boolean) => void;
    readonly setPages: (pages: boolean) => void;
    readonly setProgressbar: (progressbar: boolean) => void;
    readonly onClose: () => void;
}) => (
    <Dialog fullWidth open onClose={props.onClose} style={{ left: "50%" }}>
        <DialogContent>
            <Typography variant="h4">Runner settings</Typography>
            <Box sx={{ mt: 2 }}>
                <Typography variant="h6">Mode</Typography>
                <FormControl>
                    <RadioGroup name="mode" value={props.mode} onChange={(e: {}, mode: string) => props.setMode(mode as TModes)}>
                        <FormControlLabel key="paginated" value="paginated" control={<Radio color="primary" />} label="Paginated" />
                        <FormControlLabel key="continuous" value="continuous" control={<Radio color="primary" />} label="Continuous" />
                        <FormControlLabel key="progressive" value="progressive" control={<Radio color="primary" />} label="Progressive" />
                    </RadioGroup>
                </FormControl>
            </Box>
            <Box sx={{ mt: 2 }}>
                <Typography variant="h6">Display</Typography>
                <FormGroup>
                    <FormControlLabel
                        control={
                            <Switch
                                id="enumerators"
                                color="primary"
                                defaultChecked={props.enumerators}
                                onChange={(ev) => props.setEnumerators(ev.target.checked)}
                            />
                        }
                        label="Enumerators"
                    />
                </FormGroup>
                <FormGroup>
                    <FormControlLabel
                        control={
                            <Switch
                                id="pages"
                                color="primary"
                                defaultChecked={props.pages}
                                onChange={(ev) => props.setPages(ev.target.checked)}
                            />
                        }
                        disabled={props.mode !== "paginated"}
                        label="Page navigation"
                    />
                </FormGroup>
                <FormGroup>
                    <FormControlLabel
                        control={
                            <Switch
                                id="progressbar"
                                color="primary"
                                defaultChecked={props.progressbar}
                                onChange={(ev) => props.setProgressbar(ev.target.checked)}
                            />
                        }
                        label="Progressbar"
                    />
                </FormGroup>
            </Box>
        </DialogContent>
        <DialogActions>
            <Button onClick={props.onClose} color="primary" autoFocus>
                Close
            </Button>
        </DialogActions>
    </Dialog>
);
