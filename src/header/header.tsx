import { RefObject, MutableRefObject, useEffect, useState } from "react";
import { MUIRunner } from "../runner/runner";
import { Builder } from "@tripetto/builder";
import { TModes } from "@tripetto/runner";
import { styled } from "@mui/material/styles";
import { SettingsDialog } from "./settings";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Box from "@mui/material/Box";
import ButtonGroup from "@mui/material/ButtonGroup";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import CloseIcon from "@mui/icons-material/Close";
import CodeIcon from "@mui/icons-material/CodeRounded";
import ReadmeIcon from "@mui/icons-material/DescriptionRounded";
import ResetIcon from "@mui/icons-material/ReplayRounded";
import RestoreIcon from "@mui/icons-material/FirstPage";
import PlayIcon from "@mui/icons-material/PlayArrowRounded";
import PauseIcon from "@mui/icons-material/Pause";
import StopIcon from "@mui/icons-material/StopRounded";
import SettingsIcon from "@mui/icons-material/Settings";
import EditIcon from "@mui/icons-material/Edit";

export function Header(props: {
    readonly builder: MutableRefObject<Builder | undefined>;
    readonly runner: RefObject<MUIRunner>;
    readonly enumerators: boolean;
    readonly pages: boolean;
    readonly progressbar: boolean;
    readonly setMode: (mode: TModes) => void;
    readonly setEnumerators: (enumerators: boolean) => void;
    readonly setPages: (pages: boolean) => void;
    readonly setProgressbar: (progressbar: boolean) => void;
    readonly reset: () => void;
    readonly restore: () => void;
    readonly builderVisible: boolean;
    readonly setBuilderVisible: (show: boolean) => void;
}) {
    const [, updateHeader] = useState({});
    const [settings, setSettings] = useState(false);
    const [menu, setMenu] = useState(false);

    useEffect(() => {
        if (props.runner.current) {
            props.runner.current.onChange = () => updateHeader({});
        }
    });

    return (
        <>
            <AppBar position="static" component="nav">
                <Toolbar variant="dense">
                    <IconButton
                        size="large"
                        edge="start"
                        color="inherit"
                        aria-label="Edit"
                        sx={{ mr: 1, display: { xs: "flex", sm: "none" } }}
                        onClick={() => props.setBuilderVisible(!props.builderVisible)}
                    >
                        {props.builderVisible ? <CloseIcon /> : <EditIcon />}
                    </IconButton>
                    <Typography
                        variant="h6"
                        color="inherit"
                        component="div"
                        sx={{
                            flexGrow: 1,
                            opacity: !props.runner.current?.blocks.definition?.name ? 0.25 : undefined,
                            whiteSpace: "nowrap",
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                            maxWidth: { xs: "100%", sm: "50%" },
                        }}
                        onClick={() => props.builder.current?.edit()}
                    >
                        {props.runner.current?.blocks.definition?.name || "Unnamed form"}
                    </Typography>
                    <Button
                        color="inherit"
                        size="small"
                        href="https://gitlab.com/tripetto/examples/react-mui"
                        target="_blank"
                        sx={{ whiteSpace: "nowrap", display: { xs: "none", sm: "flex" } }}
                    >
                        <CodeIcon style={{ marginRight: 3 }} />
                        View source
                    </Button>
                    <Button
                        color="inherit"
                        size="small"
                        href="https://gitlab.com/tripetto/examples/react-mui/blob/main/README.md"
                        target="_blank"
                        sx={{ whiteSpace: "nowrap", display: { xs: "none", sm: "flex" } }}
                    >
                        <ReadmeIcon style={{ marginRight: 3 }} />
                        View readme
                    </Button>
                    <Button
                        color="inherit"
                        size="small"
                        onClick={() => props.restore()}
                        sx={{ whiteSpace: "nowrap", display: { xs: "none", sm: "flex" } }}
                    >
                        <RestoreIcon style={{ marginRight: 3 }} />
                        Restore demo
                    </Button>
                    <Box sx={{ ml: 2, display: { xs: "none", sm: "flex" } }}>
                        <ButtonGroup size="small" disabled={props.runner.current?.blocks.isPreview}>
                            <Button variant="outlined" color="inherit" title="Start form" onClick={() => props.runner.current?.start()}>
                                <PlayIcon
                                    style={{
                                        opacity:
                                            props.runner.current?.blocks.isEmpty ||
                                            (!props.runner.current?.blocks.isStopped && !props.runner.current?.blocks.isFinished)
                                                ? 0.25
                                                : 1,
                                    }}
                                />
                            </Button>
                            <Button variant="outlined" color="inherit" title="Pause form" onClick={() => props.runner.current?.pause()}>
                                <PauseIcon
                                    style={{
                                        opacity: !props.runner.current?.blocks.isRunning ? 0.25 : 1,
                                    }}
                                />
                            </Button>
                            <Button variant="outlined" color="inherit" title="Stop form" onClick={() => props.runner.current?.stop()}>
                                <StopIcon
                                    style={{
                                        opacity: !props.runner.current?.blocks.isRunning ? 0.25 : 1,
                                    }}
                                />
                            </Button>
                            <Button variant="outlined" color="inherit" title="Reset form" onClick={() => props.reset()}>
                                <ResetIcon
                                    style={{
                                        opacity: !props.runner.current?.blocks.isRunning ? 0.25 : 1,
                                    }}
                                />
                            </Button>
                        </ButtonGroup>
                    </Box>
                    <Box sx={{ ml: 2, display: { xs: "none", sm: "flex" } }}>
                        <ButtonGroup size="small" disabled={!props.runner.current?.blocks.isRunning}>
                            <Button
                                variant={!props.runner.current?.blocks.isPreview ? "contained" : "outlined"}
                                disableElevation
                                color="inherit"
                                onClick={() => {
                                    if (props.runner.current?.blocks) {
                                        props.runner.current.blocks.isPreview = false;
                                    }
                                }}
                            >
                                Run
                            </Button>
                            <Button
                                variant={props.runner.current?.blocks.isPreview ? "contained" : "outlined"}
                                disableElevation
                                color="inherit"
                                onClick={() => {
                                    if (props.runner.current?.blocks) {
                                        props.runner.current.blocks.isPreview = true;
                                    }
                                }}
                            >
                                Preview
                            </Button>
                        </ButtonGroup>
                    </Box>
                    <Box sx={{ ml: 2, display: { xs: "none", sm: "flex" } }}>
                        <IconButton size="small" color="inherit" onClick={() => setSettings(true)} disabled={settings}>
                            <SettingsIcon />
                        </IconButton>
                    </Box>
                </Toolbar>
            </AppBar>
            {settings && props.runner.current && (
                <SettingsDialog {...props} mode={props.runner.current.blocks.mode} onClose={() => setSettings(false)} />
            )}
        </>
    );
}
