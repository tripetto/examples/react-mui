import styled from "@emotion/styled";
import Alert from "@mui/material/Alert";
import { useState, useRef } from "react";
import { createRoot } from "react-dom/client";
import { IDefinition, Builder } from "@tripetto/builder";
import { TripettoBuilder } from "@tripetto/builder/react";
import { TModes, ISnapshot } from "@tripetto/runner";
import { Header } from "./header/header";
import { MUIRunner } from "./runner/runner";
import DEMO_FORM from "./demo.json";
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import "./app.css";

/** Import builder blocks */
import "./builder/blocks";

const DEFINITION_KEY = "@tripetto/example-react-mui-definition";
const DEFINTIION = (JSON.parse(localStorage.getItem(DEFINITION_KEY) || "null") || DEMO_FORM) as IDefinition;

const SNAPSHOT_KEY = "@tripetto/example-react-mui-snapshot";
const SNAPSHOT = (JSON.parse(localStorage.getItem(SNAPSHOT_KEY) || "null") || undefined) as ISnapshot;

const BuilderElement = styled.div`
    position: absolute;
    left: 0;
    right: 50%;
    top: 48px;
    bottom: 0;
    background-color: #fff;
    z-index: 1;

    @media (max-width: 991px) {
        transform: translateX(-100%);
        transition: transform 0.25s ease-out;
        right: 0;

        &.visible {
            transform: translateX(0);
        }
    }
`;

const RunnerElement = styled.div`
    position: absolute;
    left: 0%;
    right: 0;
    top: 48px;
    bottom: 0;
    overflow-x: hidden;
    overflow-y: auto;
    -webkit-overflow-scrolling: touch;
    scroll-behavior: smooth;
    background-color: #fff;
    padding: 20px;
    z-index: 0;

    @media (min-width: 992px) {
        left: 50%;
    }
`;

function App() {
    const [ready, setReady] = useState(false);
    const [mode, setMode] = useState<TModes>("paginated");
    const [enumerators, setEnumerators] = useState(false);
    const [pages, setPages] = useState(false);
    const [progressbar, setProgressbar] = useState(true);
    const [builderVisible, setBuilderVisible] = useState(false);
    const runnerRef = useRef<MUIRunner>(null);
    const builderRef = useRef<Builder>();

    return (
        <div style={{ visibility: ready ? "visible" : "hidden" }}>
            <Header
                builder={builderRef}
                runner={runnerRef}
                reset={() => {
                    runnerRef.current?.reset();

                    localStorage.removeItem(SNAPSHOT_KEY);
                }}
                restore={() => {
                    builderRef.current?.load(DEMO_FORM as {} as IDefinition);
                    runnerRef.current?.reload(DEMO_FORM as {} as IDefinition);

                    localStorage.removeItem(DEFINITION_KEY);
                    localStorage.removeItem(SNAPSHOT_KEY);
                }}
                {...{
                    setMode,
                    setEnumerators,
                    setPages,
                    setProgressbar,
                    enumerators,
                    pages,
                    progressbar,
                    builderVisible: builderVisible,
                    setBuilderVisible: setBuilderVisible,
                }}
            />
            <BuilderElement className={(builderVisible && "visible") || ""}>
                <TripettoBuilder
                    controller={builderRef}
                    definition={DEFINTIION}
                    fonts="assets/"
                    disableSaveButton={true}
                    disableRestoreButton={true}
                    disableClearButton={false}
                    disableCloseButton={true}
                    supportURL={false}
                    disableOpenCloseAnimation={true}
                    disableEpilogue={true}
                    disablePrologue={true}
                    showTutorial={!localStorage.getItem("tripetto-tutorial")}
                    zoom="fit"
                    onReady={() => setReady(true)}
                    onChange={(definition) => {
                        runnerRef.current?.reload(definition);

                        // Store the definition in the persistent local store
                        localStorage.setItem(DEFINITION_KEY, JSON.stringify(definition));
                    }}
                />
            </BuilderElement>
            <RunnerElement>
                <Alert severity="info">
                    To keep this demo as simple as possible, it only contains a selection of the{" "}
                    <a href="https://tripetto.com/sdk/docs/blocks/stock/" target="_blank">
                        stock blocks
                    </a>{" "}
                    available.
                </Alert>
                <Alert severity="warning" sx={{ mt: 1 }}>
                    This is a demonstration. No form data will ever be sent. Form data is shown in the browser console.
                </Alert>
                <MUIRunner
                    ref={runnerRef}
                    definition={DEFINTIION}
                    snapshot={SNAPSHOT}
                    mode={mode}
                    enumerators={enumerators}
                    pages={pages}
                    progressbar={progressbar}
                    onSubmit={(data) => {
                        // Output the collected data to the console
                        console.log("Form completed!");

                        data.fields.forEach((field) => {
                            if (field.string) {
                                console.log(`${field.name}: ${field.string}`);
                            }
                        });
                    }}
                    onPause={(data) => {
                        // Store the pause snapshot in the persistent local store
                        localStorage.setItem(SNAPSHOT_KEY, JSON.stringify(data));
                    }}
                />
            </RunnerElement>
        </div>
    );
}

// Let's render the App component to the root element.
createRoot(document.getElementById("root")!).render(<App />);
