## <a href="https://tripetto.com/sdk/"><img src="https://unpkg.com/@tripetto/builder/assets/header.svg" alt="Tripetto FormBuilder SDK"></a>

🙋‍♂️ The *Tripetto FormBuilder SDK* helps building **powerful and deeply customizable forms for your application, web app, or website.**

👩‍💻 Create and run forms and surveys **without depending on external services.**

💸 Developing a custom form solution is tedious and expensive. Instead, use Tripetto and **save time and money!**

🌎 Trusted and used by organizations **around the globe**, including [Fortune 500 companies](https://en.wikipedia.org/wiki/Fortune_500).

---

*This SDK is the ultimate form solution for everything from basic contact forms to surveys, quizzes and more with intricate flow logic. Whether you're just adding conversational forms to your website or application, or also need visual form-building capabilities inside your app, Tripetto has got you covered! Pick what you need from the SDK with [visual form builder](https://tripetto.com/sdk/docs/builder/introduction/), [form runners](https://tripetto.com/sdk/docs/runner/introduction/), and countless [question types](https://tripetto.com/sdk/docs/blocks/introduction/) – all with [extensive docs](https://tripetto.com/sdk/docs/). Or take things up a notch by developing your [own question types](https://tripetto.com/sdk/docs/blocks/custom/introduction/) or even [form runner UIs](https://tripetto.com/sdk/docs/runner/custom/introduction/).*

---

## 🧑‍💻 React with Material UI example
[![Read the docs](https://badgen.net/badge/icon/docs/cyan?icon=wiki&label)](https://tripetto.com/sdk/docs/)
[![Source code](https://badgen.net/badge/icon/source/black?icon=gitlab&label)](https://gitlab.com/tripetto/examples/react-mui/)
[![Follow us on Twitter](https://badgen.net/badge/icon/@tripetto?icon=twitter&label)](https://twitter.com/tripetto)

This demo shows how to build a [custom form runner](https://tripetto.com/sdk/docs/runner/custom/introduction/). A form runner turns a Tripetto form (the [form definition](https://tripetto.com/sdk/docs/builder/api/interfaces/IDefinition/)) into an actual form UI. This example uses [React](https://reactjs.org/) and [Material UI](https://mui.com/) for rendering the form. The goal is to show you how to build a custom runner with a minimal code footprint so you get a good understanding of the principles. In this example, the browser window is split in two. On the left side, you see the builder and on the right side the form (runner). You can live-edit the form in the builder and then run the form right away.

## 📺 Preview
[![Play around](https://unpkg.com/@tripetto/builder/assets/button-demo.svg)](https://tripetto.gitlab.io/examples/react-mui/)

## 💻 Run demo locally
1. [Download](https://gitlab.com/tripetto/examples/react-mui/-/archive/main/react-mui-main.zip) or clone the [repository](https://gitlab.com/tripetto/examples/react-mui.git) to your local machine:
```bash
$ git clone https://gitlab.com/tripetto/examples/react-mui.git
```

2. Run `npm install` inside the downloaded/cloned folder:
```bash
$ npm install
```

3. Start the test server and open the URL `http://localhost:9000` in the browser of your choice to show the form:
```bash
$ npm test
```

## 📖 Documentation
Tripetto has practical, extensive documentation. Find everything you need at [tripetto.com/sdk/docs/](https://tripetto.com/sdk/docs/).

## 💳 License
Have a blast. [MIT](https://opensource.org/licenses/MIT).

## 👋 About us
If you want to learn more about Tripetto or contribute in any way, visit us at [tripetto.com](https://tripetto.com/).
